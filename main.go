package main

import (
	"base/cmd"
	"base/util/slog"
	"fmt"
	"log"
	"os"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("error:%v", err)
			slog.Logger.Error("异常:", err)
		}
	}()
	client := cmd.InitCmd()
	err := client.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
