package util

import (
	"fmt"
	"os"
)

func CreateNotExist(path string) {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			err := os.MkdirAll(path, os.ModePerm)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}
