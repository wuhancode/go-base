package tools

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func CreateNotExist(path string) {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			err := os.MkdirAll(path, os.ModePerm)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	}
}

func GetPwd() string {
	file, _ := exec.LookPath(os.Args[0])
	abs, _ := filepath.Abs(file)
	index := strings.LastIndex(abs, string(os.PathSeparator))
	ret := abs[:index]
	return ret
}
