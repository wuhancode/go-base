package cmd

import (
	"base/app/router"
	"base/cron"
	"github.com/urfave/cli/v2"
)

func InitCmd() *cli.App {
	app := &cli.App{
		Name:        "命令名称",
		Description: "命令描述",
		Flags:       []cli.Flag{},
		Commands: []*cli.Command{
			{
				Name:    "task",
				Aliases: []string{"t"},
				Usage:   "定时任务脚本",
				Flags:   []cli.Flag{},
				Action: func(c *cli.Context) error {
					cron.Run()
					return nil
				},
			},{
				Name:    "web",
				Aliases: []string{"w"},
				Usage:   "网页",
				Flags:   []cli.Flag{},
				Action: func(c *cli.Context) error {
					router.Run()
					return nil
				},
			},
		},
	}
	return app
}
