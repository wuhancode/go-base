module base

go 1.12

require (
	github.com/creasty/defaults v1.5.1
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/kataras/iris/v12 v12.1.8
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.3.0
)
