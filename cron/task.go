package cron

import (
	"base/conf"
	"base/util/slog"
	"fmt"
	"time"
)

type Tasks struct{}

func (t *Tasks) TaskDemo() {
	slog.Logger.Info("测试任务开始")
	fmt.Print(conf.Conf.Timeout)
	time.Sleep(5 * time.Second)
	slog.Logger.Info("测试任务完成")
}
