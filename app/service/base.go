package service

import (
	"base/util/slog"
	"bytes"
	"os/exec"
)

func RunCmd(cmd *exec.Cmd) []byte {
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		slog.Logger.Fatal(err)
	}
	return out.Bytes()
}

func errThrow() {
	if err := recover(); err != nil {
		slog.Logger.Error("捕获异常:", err)
	}
}