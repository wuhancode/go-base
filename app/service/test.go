package service

import (
	"github.com/creasty/defaults"
	"github.com/kataras/iris/v12"
)

type Response struct {
	Code int64 `json:"code" default:"200"`
	Status string `json:"status" default:"ok"`
	Msg string `json:"msg" default:"success"`
	Data map[interface{}]interface{} `json:"data,omitempty"`
}

func Test(ctx iris.Context) {
	r := &Response{}
	defaults.Set(r)
	ctx.JSON(r)
}
