package router

import(
	"base/app/service"
	"base/conf"
	"base/util/slog"
	"fmt"
	"github.com/kataras/iris/v12"
)

func loadRouter(app *iris.Application)  {
	v1 := app.Party("/v1")
	{
		v1.Get("/test", service.Test)
	}
}

func Run()  {
	app := iris.New()
	loadRouter(app)
	err := app.Listen(fmt.Sprintf(":%s", conf.Conf.Port))
	if err != nil {
		slog.Logger.Error(err)
	}
}